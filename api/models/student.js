module.exports = function(sequelize, DataTypes) {
	var Student = sequelize.define('student', {
		first_name: DataTypes.STRING,
		last_name: DataTypes.STRING,
		personal_identification_number: DataTypes.STRING,
	});
	return Student;
};
