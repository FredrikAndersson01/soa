var db = require('../models');

exports.get = function(request, response, next) {
    db.student.findAll().then(function(students) {
		response.send(students);
        return next();
    });
};
