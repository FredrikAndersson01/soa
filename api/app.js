// Require the restify framework, controllers and models
var restify = require('restify'),
    controller = require('./controllers'),
    db = require('./models');

var server = restify.createServer();

// Allow requests from other servers
server.use(
  function crossOrigin(req,res,next){
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    return next();
  }
);

// These are the endpoints
server.get('/coursecheck/:code/:semester', controller.course.getCourseOccation);
server.get('/studentcheck/:anmkod/:studentid', controller.course.checkStudentOnCourse);
server.put('/studentreg/:studentid/:anmkod/:excode/:grade', controller.course.saveAssignmentResult);

// Sync database schema with model definitions
db.sequelize.sync({force: false}).then(function() {
    // Start the node server and listen to port 8080
    server.listen(8080, function() {
        console.log('%s listening at %s', server.name, server.url);
    });
});
