<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Registrera betyg</title>
        <script type="text/javascript" src="bower_components/jquery/dist/jquery.min.js"></script>
        <script type="text/javascript" src="bower_components/async/dist/async.min.js"></script>
        <script type="text/javascript" src="node_modules/superagent/superagent.js"></script>
        <script type="text/javascript" src="script.js"></script>
    </head>
    <body>
        <form>
            <input type="text" name="student_id" placeholder="StudentID">
            <input type="text" name="kurskod" placeholder="Kurskod">
            <input type="text" name="termin" placeholder="Termin">
            <input type="text" name="provnummer" placeholder="Provnummer">
            <input type="text" name="betyg" placeholder="Betyg">
            <input type="submit" value="Registrera betyg">
        </form>
        <script>
            var apiUrl = '<?php echo getenv('API_URL')?>';
        </script>
    </body>
</html>
