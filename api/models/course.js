module.exports = function(sequelize, DataTypes) {
	var Course = sequelize.define('course', {
		name: DataTypes.STRING,
		code: DataTypes.STRING,
		anmkod: DataTypes.STRING,
		semester: DataTypes.STRING,
	});
	return Course;
};
